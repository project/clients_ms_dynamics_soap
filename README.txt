Clients MS Dynamics SOAP
========================

Provides a Clients connection type for connecting to MS Dynamics CRM over SOAP.

Features:
  - supports connection read-only mode.
  - supports debug mode.
